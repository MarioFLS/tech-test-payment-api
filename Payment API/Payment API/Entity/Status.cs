﻿namespace Payment_API.Entity
{
    public enum Status
    {
        AWAITING_PAYMENTE,
        PAYMENT_ACCEPT,
        SENT,
        DELIVERED,
        CANCELED
    }
}
