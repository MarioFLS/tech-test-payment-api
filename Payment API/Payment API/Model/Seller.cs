﻿using System.ComponentModel.DataAnnotations;

namespace Payment_API.Model
{
    public class Seller
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O nome é obrigatório")]
        [MinLength(3, ErrorMessage = "O nome deve ter no minimo 3 caracteres")]
        public string Name { get; set; } = default!;

        [Required(ErrorMessage = "O CPF é obrigatório")]
        [MinLength(11, ErrorMessage = "O CPF deve ter no mínimo 11 números")]
        [MaxLength(11, ErrorMessage = "O CPF deve ter no máximo 11 números")]
        public string Cpf { get; set; } = default!;

        [Required(ErrorMessage = "O email é obrigatório")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Digite um email válido")]
        [RegularExpression("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,6}$", ErrorMessage = "Digite um email válido")]
        public string Email { get; set; } = default!;
        public string Phone { get; set; } = default!;
    }
}
