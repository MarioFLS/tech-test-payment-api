﻿using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Payment_API.Model
{
    public class SalesProducts
    {
        public int Id { get; set; }
        [DefaultValue(1)]
        public int Quantity { get; set; } = 1;
        public decimal Value { get; set; } = 0;

        public Product? Product { get; set; }
        [DefaultValue(1)]
        public int ProductId { get; set; } = 1;

        public void SumValue()
        {
            Value = Quantity * Product!.Price;
        }
    }
}
