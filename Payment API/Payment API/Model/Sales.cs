﻿using Newtonsoft.Json;
using Payment_API.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Payment_API.Model
{
    public class Sales
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} requerido")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }
        [JsonIgnore]
        public Seller? Seller { get; set; } = default!;
        [DefaultValue(1)]
        public int SellerId { get; set; }

        [Required(ErrorMessage = "A venda deve ter um status")]
        [EnumDataType(typeof(Status), ErrorMessage = "Digite um Status Válido")]
        public Status Status { get; set; }
        public List<SalesProducts> SalesProducts  { get; set; } = default!;
    }
}
