﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Payment_API.Services.Swagger
{
    public class SwaggerFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            schema.Properties.Remove("id");
            schema.Properties.Remove("seller");
            schema.Properties.Remove("sales");
            schema.Properties.Remove("salesId");
            schema.Properties.Remove("product");
            schema.Properties.Remove("value");
        }
    }
}
