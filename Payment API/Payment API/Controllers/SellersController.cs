﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;
using Payment_API.Model;
using Payment_API.Repository;

namespace Payment_API.Controllers
{
    [Route("seller")]
    [ApiController]
    public class SellersController : ControllerBase
    {
        private readonly PaymentContext _context;

        public SellersController(PaymentContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Seller>>> GetSellers()
        {
            return await _context.Sellers.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Seller>> GetSeller(int id)
        {
            var seller = await _context.Sellers.FindAsync(id);

            if (seller == null)
            {
                return NotFound();
            }

            return seller;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutSeller(int id, Seller seller)
        {
            try
            {
                if (id != seller.Id)
                {
                    return BadRequest();
                }

                _context.Entry(seller).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SellerExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return NoContent();
            }
            catch (DbUpdateException e)
            {

                return BadRequest("Não foi possível atualizar o Vendedor. Detalhes: " + e.InnerException!.Message);
            }
           
        }


        [HttpPost]
        public async Task<ActionResult<Seller>> PostSeller(Seller seller)
        {
            try
            {
                _context.Sellers.Add(seller);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetSeller", new { id = seller.Id }, seller);
            }
            catch (DbUpdateException e)
            {

                return BadRequest("Não foi possível criar o Vendedor. Detalhes: " + e.InnerException!.Message);
            }
            
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSeller(int id)
        {
            var seller = await _context.Sellers.FindAsync(id);
            if (seller == null)
            {
                return NotFound("Vendedor não encontrado");
            }

            _context.Sellers.Remove(seller);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SellerExists(int id)
        {
            return _context.Sellers.Any(e => e.Id == id);
        }
    }
}
