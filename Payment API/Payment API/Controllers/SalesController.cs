﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Payment_API.Entity;
using Payment_API.Model;
using Payment_API.Repository;

namespace Payment_API.Controllers
{
    [Route("sales")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly PaymentContext _context;

        public SalesController(PaymentContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Sales>>> GetSales()
        {
            return await _context.Sales.Include(e => e.SalesProducts).ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Sales>> GetSales(int id)
        {
            var sales = await _context.Sales.Include(e => e.SalesProducts).ThenInclude(e => e.Product).FirstOrDefaultAsync(e => e.Id == id);

            if (sales == null)
            {
                return NotFound();
            }

            return sales;
        }

        [HttpPut("{id}/status")]
        public async Task<IActionResult> PutStatus(int id, Status status)
        {
            Sales sale = _context.Sales.FirstOrDefault(e => e.Id == id)!;
            if (sale == null) { return NotFound(); }
            if (sale.Status == status)
            {
                return BadRequest(new { Message = "Não Pode atualizar um Status para o mesmo" });
            }
            if (sale.Status == Status.AWAITING_PAYMENTE)
            {
                if (status == Status.PAYMENT_ACCEPT || status == Status.CANCELED)
                {
                    sale.Status = status;
                    await _context.SaveChangesAsync();
                    return Ok(new { Message = "Status Alterado com Sucesso" });
                }
                return BadRequest(new { Message = "Não Pode atualizar um Status para essa opção" });
            }
            else if (sale.Status == Status.PAYMENT_ACCEPT)
            {

                if (status == Status.SENT || status == Status.CANCELED)
                {
                    sale.Status = status;
                    await _context.SaveChangesAsync();
                    return Ok(new { Message = "Status Alterado com Sucesso" });
                }
                return BadRequest(new { Message = "Não Pode atualizar um Status para essa opção" });
            }
            if (status == Status.SENT || status == Status.CANCELED)
            {
                sale.Status = status;
                await _context.SaveChangesAsync();
                return Ok(new { Message = "Status Alterado com Sucesso" });
            }
            await _context.SaveChangesAsync();
            return BadRequest(new { Message = "Não Pode atualizar um Status para essa opção" });

        }
        [HttpPost]
        public async Task<ActionResult<Sales>> PostSales(Sales sales)
        {
            Seller? seller = _context.Sellers.FirstOrDefault(e => e.Id == sales.SellerId);
            sales.SalesProducts.ForEach(e =>
            {
                Product? product = _context.Products.FirstOrDefault(p => p.Id == e.ProductId);
                e.Product = product;
                e.SumValue();
            });
            if (sales.SalesProducts.Count == 0)
            {
                return BadRequest(new { Message = "A compra deve ter pelo menos 1 item!" });
            }
            if (seller == null)
            {
                return BadRequest(new { Message = "O Vendedor não existe" });
            }
            _context.Sales.Add(sales);

            await _context.SaveChangesAsync();
            return StatusCode(201, (new { Message = "Compra feita com sucesso" }));
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSales(int id)
        {
            var sales = await _context.Sales.FindAsync(id);
            if (sales == null)
            {
                return NotFound();
            }

            _context.Sales.Remove(sales);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SalesExists(int id)
        {
            return _context.Sales.Any(e => e.Id == id);
        }
    }
}
