﻿using Microsoft.EntityFrameworkCore;
using Payment_API.Model;

namespace Payment_API.Repository
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions options) : base(options)
        {
        }

        public PaymentContext()
        {
        }

        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Sales> Sales { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().Property(e => e.Price).HasPrecision(12, 10);
            modelBuilder.Entity<SalesProducts>().Property(e => e.Value).HasPrecision(12, 10);
            modelBuilder.Entity<Seller>().HasIndex(s => s.Email).IsUnique();
            modelBuilder.Entity<Seller>().HasIndex(s => s.Cpf).IsUnique();
            modelBuilder.Entity<Seller>().HasIndex(s => s.Phone).IsUnique();

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=127.0.0.1;Database=paymentApi;User=SA;Password=Password12!;Encrypt=False");

            }
        }
    }
}

